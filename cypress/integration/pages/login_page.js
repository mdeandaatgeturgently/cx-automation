const locators = require('../../fixtures/locators/login_page_locators.json')

export const loginPage = {

    /**
     * This function is used to verify presence of all elements on login page
     */
    verifyLoginElements() {
        cy.get(locators.urgentlyLogo).should('be.visible').
            and('have.attr', 'src', 'images/logo_urgently.svg')
        cy.contains(locators.usernameLabel).should('be.visible').and('have.text', 'Username')
        cy.get(locators.usernameInput).should('be.visible')
        cy.contains(locators.passwordLabel).should('be.visible').and('have.text', 'Password')
        cy.get(locators.passwordInput).should('be.visible')
        cy.get(locators.loginButton).should('be.visible').and('have.value', 'Log In')
        cy.get(locators.forgotPasswordLink).should('be.visible').and('have.text', 'Forgot Password?').
            and('have.attr', 'ng-click')
    },

    /**
     * This function to verify presence of all elements on forgot password section
     */
    verifyForgotPasswordElements() {
        cy.get(locators.urgentlyLogo).should('be.visible').
            and('have.attr', 'src', 'images/logo_urgently.svg')
        cy.get(locators.forgotPasswordModal.header).should('be.visible').
            and('contain.text', 'Need New Password')
        cy.contains(locators.forgotPasswordModal.forgotEmailLabel).should('be.visible').
            and('have.text', 'Email Address')
        cy.get(locators.forgotPasswordModal.forgotEmailInput).should('be.visible')
        cy.get(locators.forgotPasswordModal.submitButton).should('be.visible').
            and('have.value', 'Submit')
    },

    getForgotPasswordLink() {
        return cy.get(locators.forgotPasswordLink)
    },

    /**
     * This function is used for login error message verification
     * @param {String} message 
     */
    verifyLoginErrorMessage(message) {
        cy.get(locators.loginErrorMessage).should('have.text', message)
    },

    /**
     * function enter the email and click on submit button
     * @param {String} email 
     */
    enterEmailForForgotPassword(email) {
        cy.server()
        cy.route('POST', '/v3/ops/**').as('POSTcontentLoad')
        cy.get(locators.forgotPasswordModal.forgotEmailInput).clear().type(email).should('have.value', email)
        cy.get(locators.forgotPasswordModal.submitButton).click()
        cy.wait(['@POSTcontentLoad']);
    },

    /**
     * This function is used to verify the error message for registered and not registered email 
     * @param {boolean} isRegisteredEmail whether email is registered or not
     * @param {String} message error message to be verified
     */
    verifyEmailAlertMessage(isRegisteredEmail, message) {
        if (isRegisteredEmail) {
            cy.get(locators.forgotPasswordModal.successAlertMessage).should('be.visible')
            cy.get(locators.forgotPasswordModal.successAlertMessage).should('contain.text', message)
        } else {
            cy.get(locators.forgotPasswordModal.dangerAlertMessage).should('be.visible')
            cy.get(locators.forgotPasswordModal.dangerAlertMessage).should('contain.text', message)
        }
    },

    /**
     * This method is used for login
     * @param {String} username 
     * @param {String} password 
     */
    login(username, password) {
        cy.server();
        cy.route('POST', '/v3//ops/**').as('POSTcontentLoad')
        cy.get(locators.usernameInput).type(username)
        cy.get(locators.passwordInput).type(password)
        cy.get(locators.loginButton).click()
        cy.wait(['@POSTcontentLoad']);
    }
    
}
