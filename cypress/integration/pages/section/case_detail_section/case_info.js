import { reviseVehicleDetailModal } from '../modal_popup/revise_vehicle_details_modal'

const locators = require('../../../../fixtures/locators/section/case_details_locators.json')

export const caseInfo = {
    getCustomerDetails() {
        return cy.get(locators.caseInfo.customerDetails)
    },
    getVehicleDetails() {
        return cy.get(locators.caseInfo.vehicleDetails)
    },
    getCustomFields(){
        return cy.get(locators.caseInfo.customFields)
    },
    getReviseButton() {
        return cy.get(locators.caseInfo.reviseButton)
    },
    "reviseVehicleDetailsModal": reviseVehicleDetailModal,
    verifyCustomerDetails(customerDetails) {
        const thumbnailText = customerDetails.firstName.charAt(0).toUpperCase() + customerDetails.lastName.charAt(0).toUpperCase()
        this.getCustomerDetails().prev('.thumb_Initial').should('be.visible').and('have.text', thumbnailText)
        this.getCustomerDetails().find('.name').should('contain.text', customerDetails.firstName+" "+customerDetails.lastName)
        this.getCustomerDetails().find('.number').should('have.text', customerDetails.number)
    },
    verifyVehicleDetails(vehicleDetails){
        this.getVehicleDetails().prev('.thumb_Initial').find('i').should('have.class','fa-car')
        const spanText=vehicleDetails.year.concat(" ",vehicleDetails.color," ",vehicleDetails.make," ",vehicleDetails.model)
        this.getVehicleDetails().find('span').first().should('have.text',spanText)
        this.getVehicleDetails().find('span').eq(1).should('contain.text',vehicleDetails.vin)
        //this.getVehicleDetails().find('span').eq(1).should('contain.text',vehicleDetails.licensePlate)
    },
    verifyInsuranceDetails(insuranceDetails){
        this.getCustomFields().should('be.visible').and('have.length',5)
        this.getCustomFields().eq(0).should('contain.text','Date of Loss:')
        this.getCustomFields().eq(1).should('contain.text','External Claim Number: '+insuranceDetails.claimNumber)
        this.getCustomFields().eq(2).should('contain.text','Insurance Policy Number: '+insuranceDetails.policyNumber)
        this.getCustomFields().eq(3).should('contain.text','Case Point of Contact: '+insuranceDetails.contact)
        this.getCustomFields().eq(4).should('contain.text',insuranceDetails.email)

    }
}