const locators = require('../../../../fixtures/locators/section/case_details_locators.json')

export const caseDetailRightSection = {
    getCaseServiceDropDown() {
        return cy.get(locators.rightSection.caseDropDown)
    },
    getCaseActionDropDown() {
        return cy.get(locators.rightSection.caseActionDropdown).parent()
    },
    getAuditEventList() {
        const getIframeBody = () => {
            // get the iframe > document > body
            // and retry until the body element is not empty
            return cy
            .get('iframe#reportWindow')
            .its('0.contentDocument.body').should('not.be.empty')
            // wraps "body" DOM element to allow
            // chaining more Cypress commands, like ".find(...)"
            // https://on.cypress.io/wrap
            .then(cy.wrap)
          }
        return getIframeBody().find(locators.rightSection.auditEventList)
    },
    getCustomerNotes() {
        return cy.get(locators.rightSection.customerNotes)
    },
    verifyElements() {
        this.getCaseServiceDropDown().should('be.visible')
        this.getCaseActionDropDown().should('be.visible')
        this.getCustomerNotes().should('be.visible')
    },
    verifyAuditEvent() {
        this.getCaseActionDropDown().then(($el) => {
            if ($el.text().trim() != "Notes") {
                this.getCaseActionDropDown().selectValue("Notes")
            }
            this.getCaseActionDropDown().selectValue("Job Status Detail")
        })
        this.getAuditEventList().each(($el, index, $list) => {
            cy.log($el.find('.thumb_initial img').attr('src'))
            cy.log($el.find('.sm_name').text())
            cy.log($el.find('.sm_time_left').text())
            cy.log($el.find('.message_txt').text())
        })        
    }
}