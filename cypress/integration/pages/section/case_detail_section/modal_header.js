import { chatModal } from '../modal_popup/chat_modal'
import { attachDocumentModal } from '../modal_popup/attach_document_modal'
import { restrictedHighwayModal } from '../modal_popup/restricted_highway_modal'

const locators = require('../../../../fixtures/locators/section/case_details_locators.json')

export const modalHeader = {
    getPartnerName() {
        return cy.get(locators.modalHeader.partnerName)
    },
    getCaseNumber() {
        return cy.get(locators.modalHeader.caseNumber)
    },
    getCaseStatus() {
        return cy.get(locators.modalHeader.caseStatus)
    },
    getChatButton(isCSSChat=false) {
        if(isCSSChat) return cy.get(locators.modalHeader.chatButtonCSS)
        return cy.get(locators.modalHeader.chatButton)
    },
    getDetailedInfoHover() {
        return cy.get(locators.modalHeader.detailedInfo)
    },
    getCaseActionDropDown() {
        return cy.get(locators.modalHeader.caseAction)
    },
    getCloseIcon() {
        return cy.get(locators.modalHeader.closeIcon)
    },
    "chatModal": chatModal,
    "attachDocumentModal": attachDocumentModal,
    "restrictedHighwayModal": restrictedHighwayModal,
    verifyModalHeaderElements() {
        this.getPartnerName().should('be.visible')
        this.getCaseNumber().should('be.visible')
        this.getCaseStatus().should('be.visible')
        this.getChatButton().should('be.visible')
        this.getCaseActionDropDown().should('be.visible')
        this.getCloseIcon().should('be.visible')
    },
    verifyDetailedInfoOnCaseHover(caseDetails) {
        this.getDetailedInfoHover().should('be.visible')
        this.getDetailedInfoHover().find('.dataset').should('have.length', 4).each(($el, index, $list) => {
            expect($el.find('.title').text().trim()).to.be.equal(caseDetails[index].label)
            if(index==1 || index==3){
                expect($el.find('.data').text().trim()).to.match(new RegExp(caseDetails[index].data))
            }else{
                expect($el.find('.data').text().trim()).to.be.equal(caseDetails[index].data)
            }
        })
    }
}