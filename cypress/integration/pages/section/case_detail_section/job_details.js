import { reviseVehicleLocationModal } from '../modal_popup/revise_vehicle_location'
import { viewHistoryModal } from '../modal_popup/view_history_modal'
import { viewInspectionModal } from '../modal_popup/view_inspection_modal'
import { cancelJobModal } from '../modal_popup/cancel_job_modal'
import { attachDocumentModal } from '../modal_popup/attach_document_modal'

const locators = require('../../../../fixtures/locators/section/case_details_locators.json')

export const jobDetails = {

    getJobServiceName() {
        return cy.get(locators.jobDetail.serviceName)
    },
    getJobNumber() {
        return cy.get(locators.jobDetail.jobNumber)
    },
    getJobStatus() {
        return cy.get(locators.jobDetail.jobStatus)
    },
    getJobAction() {
        return cy.get(locators.jobDetail.jobAction)
    },
    getSubHeader(header) {
        return cy.contains(locators.jobDetail.subHeader,header)
    },
    getDetailedInfoHover(){
        return cy.get(locators.jobDetail.detailedInfo)
    },
    "cancelJobModal": cancelJobModal,
    "viewHistoryModal": viewHistoryModal,
    "attachDocumentModal": attachDocumentModal,
    "viewInspectionModal": viewInspectionModal,
    "providerSection": {
        getRevisePickUpButton() {
            return cy.get(locators.jobDetail.provider.revisePickUpButton)
        },
        getReviseDropOffLocation() {
            return cy.get(locators.jobDetail.provider.reviseDropOffButton)
        },
        getDriverProviderDetails() {
            return cy.get(locators.jobDetail.provider.nameNumberRow)
        },
        getLocationETADetails() {
            return cy.get(locators.jobDetail.provider.locationAndETARow)
        },
        "reviseVehicleLocationModal": reviseVehicleLocationModal
    },
    "mapSection": {
        getJobDetailsMap() {
            return cy.get(locators.jobDetail.map.jobDetailMap)
        },
        getInfoBoxOnMap(){
            return cy.get(locators.jobDetail.map.mapInfoBox)
        }
    },
    "documentSection": {
        getNoDocumentText() {
            return cy.get(locators.jobDetail.documents.noDocumentText)
        },
        getUploadedDocument() {
            return cy.get(locators.jobDetail.documents.uploadedDocument)
        }
    },
    verifyDetailInfoOnJobNameHover(caseDetails,jobIndex){
        this.getJobServiceName().eq(jobIndex).trigger('mouseover', 'center')
        this.getDetailedInfoHover().eq(jobIndex).should('be.visible').find('.dataset').should('have.length', 2).each(($el, index, $list) => {
            expect($el.find('.title').text().trim()).to.be.equal(caseDetails[index].label)
            if(index==1){
                expect($el.find('.data').text().trim()).to.match(new RegExp(caseDetails[index].data))
            }else{
                expect($el.find('.data').text().trim()).to.be.equal(caseDetails[index].data)
            }
        })
        
    }

}