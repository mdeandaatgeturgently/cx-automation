const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const cancelJobModal = {
    verifyHeader(header) {
        cy.get(locators.cancelJobModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.cancelJobModal.closeIcon)
    },
    getCancelButton(){
        return cy.get(locators.cancelJobModal.cancelButton)
    },
    getSaveButton(){
        return cy.get(locators.cancelJobModal.saveButton)
    },
    getCancelJobLabel(){
        return cy.get(locators.cancelJobModal.cancelJobLabel)
    },
    getCancelJobReason(reason){
        return cy.get(locators.cancelJobModal.jobCancelReason).contains(reason)
    },
    getNoteText(){
        return cy.get(locators.cancelJobModal.noteText)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getCancelButton().should('be.visible')
        this.getSaveButton().should('be.enabled')
        this.getCancelJobLabel().should('be.visible')
    }
}