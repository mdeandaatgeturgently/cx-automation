const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const viewInspectionModal = {
    verifyHeader(header) {
        cy.get(locators.viewInspectionModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.viewInspectionModal.closeIcon)
    },
    getCloseButton(){
        return cy.get(locators.viewInspectionModal.closeButton)
    },
    getNoInspectionText(){
        return cy.get(locators.viewInspectionModal.noInspectionText)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getCloseButton().should('be.visible')
        this.getNoInspectionText().should('be.visible')
    }
}