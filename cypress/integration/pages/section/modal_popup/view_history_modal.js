const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const viewHistoryModal = {
    verifyHeader(header) {
        cy.get(locators.viewHistoryModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.viewHistoryModal.closeIcon)
    },
    getHistoryTimelineList(){
        return cy.get(locators.viewHistoryModal.historyTimeLine)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getHistoryTimelineList().should('be.visible')
    }
}