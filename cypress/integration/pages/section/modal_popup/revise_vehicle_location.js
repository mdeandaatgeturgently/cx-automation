const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const reviseVehicleLocationModal = {
    verifyHeader(header) {
        cy.get(locators.reviseVehicleLocationModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.reviseVehicleLocationModal.closeIcon)
    },
    getCancelButton(){
        return cy.get(locators.reviseVehicleLocationModal.cancelButton)
    },
    getReviseButton(){
        return cy.get(locators.reviseVehicleLocationModal.reviseButton)
    },
    getPickUpLocationInput(){
        return cy.get(locators.reviseVehicleLocationModal.pickUpLocationInput)
    },
    getOtherDropOffLocationInput(){
        return cy.get(locators.reviseVehicleLocationModal.dropOffLocationInput)
    },
    getDropOffLocationDropDown(){
        return cy.get(locators.reviseVehicleLocationModal.dropOffLocationDropDown)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getCancelButton().should('be.visible')
        this.getReviseButton().should('be.visible')
    }
}