const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const reviseVehicleDetailModal = {
    verifyHeader(header) {
        cy.get(locators.reviseVehicleDetailModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.reviseVehicleDetailModal.closeIcon)
    },
    getCloseButton(){
        return cy.get(locators.reviseVehicleDetailModal.closeButton)
    },
    getVehicleMake(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleMake)
    },
    getVehicleModel(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleModel)
    },
    getVehicleYear(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleYear)
    },
    getVehicleColor(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleColor)
    },
    getVehicleLicensePlate(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleLicensePlate)
    },
    getVehicleVin(){
        return cy.get(locators.reviseVehicleDetailModal.vehicleVin)
    },
    getReviseButton(){
        return cy.get(locators.reviseVehicleDetailModal.reviseButton)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getVehicleMake().should('be.visible').and('be.disabled')
        this.getVehicleModel().should('be.visible').and('be.disabled')
        this.getVehicleYear().should('be.visible').and('be.disabled')
        this.getVehicleColor().should('be.visible').and('be.disabled')
        this.getVehicleLicensePlate().should('be.visible').and('be.enabled')
        this.getVehicleVin().should('be.visible').and('be.disabled')
        this.getCloseButton().should('be.visible')
        this.getReviseButton().should('be.visible').and('be.enabled')
    },
    verifyFieldValues(vehicleDetails){
        this.getVehicleMake().should('have.value', vehicleDetails.make)
        this.getVehicleModel().should('have.value', vehicleDetails.model)
        this.getVehicleYear().find('span').first().should('have.text', vehicleDetails.year)
        this.getVehicleColor().find('span').first().should('have.text', vehicleDetails.color)
        this.getVehicleVin().should('have.value', vehicleDetails.vin)
    }
}