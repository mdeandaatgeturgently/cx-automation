const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const restrictedHighwayModal = {
    verifyHeader(header) {
        cy.get(locators.restrictedHighwayModal.header).should('contain.text', header)
    },
    getCloseButton() {
        return cy.get(locators.restrictedHighwayModal.closeButton)
    },
    getCurrentStateLabel() {
        return cy.get(locators.restrictedHighwayModal.labelCurrentState)
    },
    getCurrentStateButton() {
        return cy.get(locators.restrictedHighwayModal.currenStateDropDownButton)
    },
    getStateDropdownList() {
        return cy.get(locators.restrictedHighwayModal.stateListDropdown)
    },
    getParagraphText() {
        return cy.get(locators.restrictedHighwayModal.text)
    },
    getCloseIcon() {
        return cy.get(locators.restrictedHighwayModal.closeIcon)
    },
    /**
     * Verify all elements on restricted highway modal 
     */
    verifyModalElements() {
        this.getCurrentStateLabel().should('be.visible')
        this.getCurrentStateButton().should('be.visible')
        this.getStateDropdownList().should('not.be.visible')
        this.getParagraphText().should('be.visible')
        this.getCloseButton().should('be.visible')
        this.getCloseIcon().should('be.visible')
    },
    /**
     * Verify paragraph text for each state
     * @param {Array} stateList 
     */
    verifyParagraphTextForEachState(stateList) {
        this.getStateDropdownList().should('have.length', stateList.length)
        stateList.forEach((state) => {
            this.getCurrentStateButton().click()
            this.getStateDropdownList().should('be.visible')
            this.getStateDropdownList().contains(state.name).scrollIntoView({ duration: 2000 }).click()
            this.getCurrentStateButton().should('have.text', state.name)
            this.getParagraphText().invoke('text').then((text)=>{
                expect(text.trim()).to.contain(state.text)      
            })
        })
    }
}