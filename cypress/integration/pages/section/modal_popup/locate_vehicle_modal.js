const locators=require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const locateVehicleModal = {
    verifyHeader(header){
        cy.get(locators.locateVehicleModal.header).should('have.text',header)
    },
    getPhoneNumber(){
        return cy.get(locators.locateVehicleModal.phoneNumber)
    },
    getText(){
        return cy.get(locators.locateVehicleModal.text)
    },
    getSendButton(){
        return cy.get(locators.locateVehicleModal.sendButton)
    },
    getCancelButton(){
        return cy.get(locators.locateVehicleModal.cancelButton)
    },
    getCloseIcon(){
        return cy.get(locators.locateVehicleModal.closeIcon)
    },
    verifyModalElements(){
        this.getPhoneNumber().should('be.visible')
        this.getText().should('be.visible')
        this.getSendButton().should('be.visible')
        this.getCancelButton().should('be.visible')
        this.getCloseIcon().should('be.visible')
    }



}