const locators=require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const closeAddCaseModal = {
    verifyHeader(header){
        cy.get(locators.closeCaseModal.header).should('contain.text',header)
    },
    getYesCloseButton(){
        return cy.get(locators.closeCaseModal.closeButton)
    },
    getCancelButton(){
        return cy.get(locators.closeCaseModal.cancelButton)
    },
    getCloseIcon(){
        return cy.get(locators.closeCaseModal.closeIcon)
    },
    verifyModalElements(){
        this.getCancelButton().should('be.visible')
        this.getYesCloseButton().should('be.visible')
        this.getCloseIcon().should('be.visible')
    }
}