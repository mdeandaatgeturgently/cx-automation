const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const chatModal = {
    verifyHeader(header) {
        cy.get(locators.chatModal.header).should('have.text', header)
    },
    getCloseIcon() {
        return cy.get(locators.chatModal.closeIcon)
    },
    getCloseButton(){
        return cy.get(locators.chatModal.closeButton)
    },
    getSendButton(){
        return cy.get(locators.chatModal.sendButton)
    },
    getChatTextArea(){
        return cy.get(locators.chatModal.chatTextArea)
    },
    getChatMessages(){
        return cy.get(locators.chatModal.chatMessages)
    },
    verifyModalElements() {
        this.getCloseIcon().should('be.visible')
        this.getCloseButton().should('be.visible')
        this.getSendButton().should('be.disabled')
    }
}