const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const attachDocumentModal = {
    verifyHeader(header) {
        cy.get(locators.attachDocumentModal.header).should('have.text', header)
    },
    getDocumentName() {
        return cy.get(locators.attachDocumentModal.name)
    },
    getDocumentDescription() {
        return cy.get(locators.attachDocumentModal.description)
    },
    getFileUploadButton() {
        return cy.get(locators.attachDocumentModal.fileUpload)
    },
    getCancelButton() {
        return cy.get(locators.attachDocumentModal.cancelButton)
    },
    getAttachDocumentButton() {
        return cy.get(locators.attachDocumentModal.attachButton)
    },
    getCloseIcon() {
        return cy.get(locators.attachDocumentModal.closeIcon)
    },
    verifyModalElements() {
        this.getDocumentName().should('be.visible')
        this.getDocumentDescription().should('be.visible')
        this.getFileUploadButton().should('be.visible')
        this.getCancelButton().should('be.visible')
        this.getAttachDocumentButton().should('be.visible')
        this.getCloseIcon().should('be.visible')
    },
    uploadDocument(fileName = 'upload-test-file',description='Document uploaded from Add Case') {
        cy.fixture('data/common/uploadTestFile.txt').then((fileContent) => {
            cy.get('[type="file"]').attachFile({
                fileContent: fileContent.toString(),
                fileName: fileName + '.txt',
                mimeType: 'text/plain'
            })
        })
        this.getDocumentName().type(fileName)
        this.getDocumentDescription().type(description)
        this.getAttachDocumentButton().click()
    }

}