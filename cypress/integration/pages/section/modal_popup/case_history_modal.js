const locators = require('../../../../fixtures/locators/section/modal_popup_locators.json')

export const caseHistoryModal = {
    getHeader() {
        return cy.get(locators.caseHistoryModal.modalHeader)
    },
    getCustomerName(){
        return cy.get(locators.caseHistoryModal.customerName)
    },
    getJobList(){
        return cy.get(locators.caseHistoryModal.jobList)
    },
    getCloseIcon() {
        return cy.get(locators.caseHistoryModal.closeIcon)
    },
    verifyModalElements() {
        this.getHeader().should('be.visible')
        this.getCustomerName().should('be.visible')
        this.getJobList().should('be.visible')
        this.getCloseIcon().should('be.visible')
    }
}