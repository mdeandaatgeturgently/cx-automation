const locators = require('../../../../fixtures/locators/section/add_case_locators.json')

const getPartner = () => Cypress.env('partner')

export const paymentInformationSection = {
    getCustomerPaymentLabel() {
        return cy.get(locators.paymentInfo.customerPaymentLabel)
    },
    getPaymentDropDown() {
        let locator = locators.paymentInfo.paymentDropDown
        if (getPartner() == 'bmw' || getPartner() == 'fleet' || getPartner() == 'jlr') {
            locator = locators.paymentInfo[getPartner()].paymentDropDown
        }
        return cy.get(locator)
    },
    getCreditCardNumber() {
        return cy.get(locators.paymentInfo.creditCardNumber)
    },
    getCreditCardExpiryMonth() {
        return cy.get(locators.paymentInfo.creditCardExpiryMonth)
    },
    getCreditCardExpiryYear() {
        return cy.get(locators.paymentInfo.creditCardExpiryYear)
    },
    getCreditCardSecurityCode() {
        return cy.get(locators.paymentInfo.creditCardSecurityCode)
    },
    getCreditCardZipCode() {
        return cy.get(locators.paymentInfo.creditCardZipCode)
    },
    getAuthorizeCardButton() {
        return cy.get(locators.paymentInfo.authorizeCardButton)
    },
    getDiscountReasonDropdown() {
        return cy.get(locators.paymentInfo.selectDiscountReasonDropdown)
    },
    getDiscountComment() {
        return cy.get(locators.paymentInfo.discountComment)
    },
    getEmailReceiptLabel() {
        return cy.get(locators.paymentInfo.emailReceiptLabel)
    },
    getEmail() {
        return cy.get(locators.paymentInfo.emailInput)
    },
    getPaymentAuthCode() {
        let locator
        if (getPartner() == 'fleet') {
            locator = locators.paymentInfo[getPartner()].paymentAuthCode
        }
        return cy.get(locator)
    },
    verifyCreditCardElements() {
        this.getPaymentDropDown().should('have.text', 'Credit Card')
        this.getCreditCardNumber().should('be.visible')
        this.getCreditCardExpiryMonth().should('be.visible')
        this.getCreditCardExpiryYear().should('be.visible')
        this.getCreditCardSecurityCode().should('be.visible')
        this.getCreditCardZipCode().should('be.visible')
        this.getAuthorizeCardButton().should('be.visible')
    },
    fillCreditCardDetails(paymentInformation) {
        this.getCreditCardNumber().type(paymentInformation.ccNumber)
        this.getCreditCardExpiryMonth().selectValue(paymentInformation.expMonth)
        this.getCreditCardExpiryYear().selectValue(paymentInformation.expYear)
        this.getCreditCardSecurityCode().type(paymentInformation.securityCode)
        this.getCreditCardZipCode().type(paymentInformation.zipCode)
        this.getAuthorizeCardButton().not('[disabled]').click()
        cy.wait(['@POSTcontentLoad'])
    },
    verifyDiscountElements() {
        this.getPaymentDropDown().should('have.text', 'Customer Discount')
        this.getDiscountReasonDropdown().should('be.visible')
        this.getDiscountComment().should('be.visible')
    },
    verifyDiscountDropDownList(discountList) {
        this.getDiscountReasonDropdown().should('be.visible')
        this.getDiscountReasonDropdown().click()
        this.getDiscountReasonDropdown().should('have.length', discountList.length)
            .each(($el, index, $list) => {
                cy.wrap($el).scrollIntoView({ duration: 2000 })
                cy.wrap($el).invoke('text').should('be.oneOf', discountList)
            })
    }

}