import { restrictedHighwayModal } from '../modal_popup/restricted_highway_modal'

const locators = require('../../../../fixtures/locators/section/add_case_locators.json')

const getPartner = () => Cypress.env('partner')

export const serviceInformationSection = {
    getServiceQuestions() {
        let locator = locators.serviceInfo.questions
        if (getPartner() == 'bmw' || getPartner() == 'volvo' || getPartner() == 'fleet') {
            locator = locators.serviceInfo[getPartner()].questions
        }
        return cy.get(locator)
    },
    getDropOffLocation() {
        return cy.get(locators.serviceInfo.dropOffLocation)
    },
    getScheduleJobNow() {
        return cy.get(locators.serviceInfo.scheduleJobNow)
    },
    getScheduleJobLater() {
        let locator = locators.serviceInfo.scheduleJobLater
        if (getPartner() == 'bmw' || getPartner() == 'volvo' || getPartner() == 'fleet') {
            locator = locators.serviceInfo[getPartner()].scheduleJobLater
        }
        return cy.get(locator)
    },
    getScheduleType() {
        return cy.get(locators.serviceInfo.scheduleType)
    },
    getScheduleJobDate() {
        return cy.get(locators.serviceInfo.scheduleDate)
    },
    getSelectServiceTypeButton() {
        return cy.get(locators.serviceInfo.selectServiceType)
    },
    getSelectReasonForDisablement() {
        return cy.get(locators.serviceInfo.selectReasonForDisablement)
    },
    getTowDropOffLocation() {
        return cy.get(locators.serviceInfo.towDropOffLocation)
    },
    getCollisionDropOffLocation() {
        return cy.get(locators.serviceInfo.collisionDropOffLocation)
    },
    getSelectServiceType() {
        return cy.get(locators.serviceInfo.serviceType)
    },
    getDisablementReason() {
        return cy.get(locators.serviceInfo.reasonForDisablement)
    },
    getRestrictedHighway() {
        return cy.get(locators.serviceInfo.restrictedHighway)
    },
    getPickUpLocation(){
        return cy.get(locators.serviceInfo.bmw.pickupLocation)
    },
    getBreakRoutingAssitanceLink(){
        return cy.get(locators.serviceInfo.breakDownRoutingAssistanceLink)
    },
    getLocateVehicle(){
        return cy.get(locators.serviceInfo.locateVehicle)
    },
    getCaliberLocationList() {
        return cy.get(locators.vehicleInformation.caliberLocationList)
    },
    getTireServiceError(){
        return cy.get(locators.serviceInfo.tireServiceError)
    },
    getOtherLocation() {
        return cy.get(locators.serviceInfo.otherLocation)
    },
    "restrictedHighwayModal": restrictedHighwayModal,
    verifyServiceQuestionAndSelectAnswer(serviceQuestionList) {
        this.getServiceQuestions().should('have.length', serviceQuestionList.length)
        this.getServiceQuestions().each(($el, index, $list) => {
            let question = $el.prev('.pt9').text().trim()
            expect(question).to.be.equal(serviceQuestionList[index].question)
            let answer = serviceQuestionList[index].answer
            cy.wrap($el).contains(answer, { matchCase: false }).click()
        })
    },
    verifyDropOffFacilityIdSearch(searchText) {
        this.getCollisionDropOffLocation().type(searchText)
        cy.wait(['@GETcontentLoad'])
        this.getCaliberLocationList().should('be.visible').and('have.length.gte', 1)
        this.getCaliberLocationList().each(($el, index, $list) => {
            const locationText = $el.text().trim()
            if (locationText != "Other Location") {
                expect(locationText).to.contain(searchText)
            }
        })
        this.getCaliberLocationList().eq(0).click()
    },
    selectDateFromDatePicker(date) {
        this.getScheduleJobLater().click()
        this.getScheduleJobDate().click()
        cy.get('[data-action="selectDay"]').not('.disabled').contains(date).should('not.have.class', 'disabled').click({ force: true })
        this.getScheduleJobDate().click()
    }
}