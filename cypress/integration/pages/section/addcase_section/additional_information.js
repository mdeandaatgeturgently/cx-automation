import { attachDocumentModal } from '../modal_popup/attach_document_modal'
import { customerInformationSection } from './customer_information'

const locators = require('../../../../fixtures/locators/section/add_case_locators.json')

const getPartner = () => Cypress.env('partner')

export const additionalInformationSection = {
    "addDocument": {
        getUploadDocumentText() {
            return cy.get(locators.additionalInformation.documentation.uploadDocumentText)
        },
        getUploadedDocumentName() {
            return cy.get(locators.additionalInformation.documentation.uploadedDocumentName)
        },
        getDeleteUploadedFile() {
            return cy.get(locators.additionalInformation.documentation.deleteUploadedFileButton)
        },
        getAddDocumentButton() {
            return cy.get(locators.additionalInformation.documentation.addDocumentButton)
        },
        "attachDocumentModal": attachDocumentModal
    },
    "vehicleOwner": {
        getFirstName() {
            let locator = locators.additionalInformation.vehicleOwner.firstName
            if (getPartner() == 'volvo') {
                locator = locators.additionalInformation.vehicleOwner[getPartner()].firstName
            }
            return cy.get(locator)
        },
        getLastName() {
            let locator = locators.additionalInformation.vehicleOwner.lastName
            if (getPartner() == 'volvo') {
                locator = locators.additionalInformation.vehicleOwner[getPartner()].lastName
            }
            return cy.get(locator)
        },
        getCustomerPhone() {
            let locator = locators.additionalInformation.vehicleOwner.customerPhone
            if (getPartner() == 'volvo' || getPartner() == 'fleet') {
                locator = locators.additionalInformation.vehicleOwner[getPartner()].customerPhone
            }
            return cy.get(locator)
        },
        getPolicyNumber() {
            let locator
            if (getPartner() == 'volvo') {
                locator = locators.additionalInformation.vehicleOwner[getPartner()].policyNumber
            }
            return cy.get(locator)
        },
        fillVehicleOwnerDetails(customerInformation) {
            this.getFirstName().type(customerInformation.firstName)
            this.getLastName().type(customerInformation.lastName)
            this.getCustomerPhone().type(customerInformation.customerPhone)
        },
    },
    "claimInformation": {
        getPolicyNumber() {
            return cy.get(locators.additionalInformation.claim.policyNumber)
        },
        getClaimNumber() {
            return cy.get(locators.additionalInformation.claim.claimNumber)
        },
        getCollisionClaimNumber() {
            return cy.get(locators.additionalInformation.claim.collisionClaimNumber)
        },
        getInsuranceCarrier() {
            return cy.get(locators.additionalInformation.claim.insuranceCarrier)
        },
        getExposureNumber() {
            return cy.get(locators.additionalInformation.claim.exposureNumber)
        },
        getDateOfLoss() {
            return cy.get(locators.additionalInformation.claim.dateOfLoss)
        },
        getBillingCenterId() {
            return cy.get(locators.additionalInformation.claim.billingCenterId)
        },
        fillPolicyDetails(claimInformation) {
            this.getPolicyNumber().type(claimInformation.policyNumber)
            cy.wait(['@POSTcontentLoad'])
            this.getClaimNumber().type(claimInformation.claimNumber)
        }
    }
}