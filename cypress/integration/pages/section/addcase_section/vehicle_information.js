import { restrictedHighwayModal } from '../modal_popup/restricted_highway_modal'
import { locateVehicleModal } from '../modal_popup/locate_vehicle_modal'
import { homePage } from '../../home_page'

const locators = require('../../../../fixtures/locators/section/add_case_locators.json')

const getPartner = () => Cypress.env('partner')

export const vehicleInformationSection = {

    getVinNumber() {
        let locator = locators.vehicleInformation.vinInput
        if (getPartner() == 'bmw' || getPartner() == 'fleet' || getPartner() == 'jlr') {
            locator = locators.vehicleInformation[getPartner()].vin
        }
        return cy.get(locator)
    },
    getVinSearchButton() {
        let locator = locators.vehicleInformation.vinSearch
        if (getPartner() == 'bmw' || getPartner() == 'fleet') {
            locator = locators.vehicleInformation[getPartner()].vinSearch
        }
        return cy.get(locator)
    },
    getOdoMeter(){
        return cy.get(locators.vehicleInformation.odoMeter)
    },
    getFindButton(){
        return cy.get(locators.vehicleInformation.findButton)
    },
    getBrandName() {
        return cy.get(locators.vehicleInformation[getPartner()].brandName)
    },
    getBrandMenuList(){
        return cy.get(locators.vehicleInformation.brandMenuList)
    },
    getVinErrorLabel() {
        return cy.get(locators.vehicleInformation.vinErrorLabel)
    },
    getVinSearchList() {
        return cy.get(locators.vehicleInformation.vinSearchLabelList)
    },
    getVinStatusLabel() {
        return cy.get(locators.vehicleInformation.vinStatusLabel)
    },
    getVinSearchLabel(isValidVIN) {
        return cy.get(isValidVIN ? locators.vehicleInformation.vinFoundLabel : locators.vehicleInformation.vinNotFoundLabel,{timeout:50000})
    },
    getFieldLabels() {
        return cy.get(locators.vehicleInformation.fieldLabelList)
    },
    getVehicleName() {
        return cy.get(locators.vehicleInformation.vehicleName)
    },
    getVehicleEditButton() {
        return cy.get(locators.vehicleInformation.vehicleEditButton)
    },
    getVehicleMake() {
        let locator = locators.vehicleInformation.vehicleMake
        if (getPartner() == 'volvo') {
            locator = locators.vehicleInformation[getPartner()].vehicleMake
        }
        return cy.get(locator)
    },
    getVehicleModel() {
        let locator = locators.vehicleInformation.vehicleModel
        if (getPartner() == 'volvo') {
            locator = locators.vehicleInformation[getPartner()].vehicleModel
        }
        return cy.get(locator)
    },
    getVehicleYear() {
        let locator = locators.vehicleInformation.vehicleYear
        if (getPartner() == 'volvo' || getPartner() == 'fleet') {
            locator = locators.vehicleInformation[getPartner()].vehicleYear
        }
        return cy.get(locator)
    },
    getVehicleClass() {
        let locator = locators.vehicleInformation.vehicleClass
        if (getPartner() == 'fleet') {
            locator = locators.vehicleInformation[getPartner()].vehicleClass
        }
        return cy.get(locator)
    },
    getLicencePlate() {
        return cy.get(locators.vehicleInformation.licensePlate)
    },
    getVehicleColor() {
        let locator = locators.vehicleInformation.vehicleColor
        if (getPartner() == 'volvo' || getPartner() == 'fleet') {
            locator = locators.vehicleInformation[getPartner()].vehicleColor
        }
        return cy.get(locator)
    },
    getBussinessName() {
        return cy.get(locators.vehicleInformation.bussinessName)
    },
    getVehicleLocated() {
        return cy.get(locators.vehicleInformation.vehicleLocated)
    },
    getPickUpLocation() {
        let locator = locators.vehicleInformation.vehiclePickUpLocation
        if (getPartner() == 'fleet') {
            locator = locators.vehicleInformation[getPartner()].vehiclePickUpLocation
        }
        return cy.get(locator)
    },
    getDropOffLocation() {
        return cy.get(locators.vehicleInformation.vehicleDropOffLocation)
    },
    getDropLocationInput() {
        return cy.get(locators.vehicleInformation.dropLocationInput)
    },
    getCustomerNotes() {
        return cy.get(locators.vehicleInformation.customerNotes)
    },
    getRestrictedHighway() {
        return cy.get(locators.vehicleInformation.restrictedHighway)
    },
    getErrorFieldsMessage() {
        return cy.get(locators.vehicleInformation.errorFieldMessage)
    },
    getCaliberLocationList() {
        return cy.get(locators.vehicleInformation.caliberLocationList)
    },
    getRadioButtonLabel() {
        return cy.get(locators.vehicleInformation.radioButtonLabel)
    },
    getLocateVehicle() {
        return cy.get(locators.vehicleInformation.locateVehicle)
    },
    getPhoneNumber() {
        return cy.get(locators.vehicleInformation.phoneNumber)
    },
    getCustomerSearch() {
        return cy.get(locators.vehicleInformation[getPartner()].customerSearch)
    },
    getCustomerRadio() {
        return cy.get(locators.vehicleInformation[getPartner()].customerRadio)
    },
    getVehicleDropDown() {
        return cy.get(locators.vehicleInformation[getPartner()].selectVehicleDropDown)
    },
    getDriverDropDown() {
        return cy.get(locators.vehicleInformation[getPartner()].selectDriverDropDown)
    },
    "restrictedHighwayModal": restrictedHighwayModal,
    "locateVehicleModal": locateVehicleModal,
    verifyVinSearchResult(customerDetails, vinStatus) {
        this.getVinSearchList().each(($el, index, $list) => {
            const label = $el.text().trim().toLowerCase()
            cy.wrap($el.next()).should('contain.text', customerDetails[label])
        })
        this.getVinStatusLabel().should('have.text', vinStatus)
    },
    fillJLRInformation(vehicleInformation){
        this.getBrandName().typeAndSelectValue(vehicleInformation.brandName)
        this.getVinNumber().type(vehicleInformation.vin)
        this.getOdoMeter().type(vehicleInformation.odoMeter)
        this.getFindButton().click()
        cy.wait(['@GETcontentLoad'])
        homePage.addCaseModal.getNextButton().not('[disabled]').click()
    },
    //for caliber collision only
    verifyPickupFacilityIdSearch(searchText) {
        this.getPickUpLocation().type(searchText)
        cy.wait(['@GETcontentLoad'])
        this.getCaliberLocationList().should('be.visible').and('have.length.gte', 1)
        this.getCaliberLocationList().each(($el, index, $list) => {
            expect($el.text().trim()).to.contain(searchText)
        })
        this.getCaliberLocationList().eq(0).click()
    },

    fillVerticalVehicleInformation_V2(vehicleInformation,isValidVin=true){
        this.getVinNumber().clear().type(vehicleInformation.vin+"{enter}")
        cy.wait(['@GETcontentLoad'])
        if(!isValidVin){
            this.getVinErrorLabel().contains('VIN not found')
            this.getVehicleMake().clear().type('Volvo{enter}')
            this.getVehicleModel().clear().type('V40{enter}')
            this.getVehicleYear().clear().type('2020{enter}')
        }
        this.getVehicleClass().selectValue(vehicleInformation.vehicleClass)
        cy.wait(['@GETcontentLoad'])
        this.getVehicleColor().clear().type(vehicleInformation.vehicleColor)
        let jobType=vehicleInformation.jobType
        cy.contains(locators.vehicleInformation.radioButtonLabel,jobType).should('be.visible').click()
        //this.getRadioButtonLabel().contains(jobType).click()
        if (jobType == 'Impound/Tow Storage' || jobType == 'Impound/Storage') {
            this.getBussinessName().type(vehicleInformation.bussinessName)
        }
        this.getPickUpLocation().type(vehicleInformation.pickUpLocation+"{enter}", { force: true })
        cy.wait(['@GETcontentLoad'])
    },
    //for collision and amfam
    fillVerticalVehicleInformation(jobType, vehicleInformation, locationInformation, vinValid = true) {
        if (!vinValid) {
            vehicleInformation.vin = "qwer231233weqe{enter}"
        }
        this.getVinNumber().clear().type(vehicleInformation.vin)
        cy.wait(['@GETcontentLoad'])
        if (!vinValid) {
            this.getVinErrorLabel().contains('VIN not found')
            this.getVehicleMake().clear().type('honda{enter}')
            this.getVehicleModel().clear().type('civic{enter}')
            this.getVehicleYear().clear().type('2020{enter}')
        }
        this.getVehicleClass().selectValue(vehicleInformation.vehicleClass)
        cy.wait(['@GETcontentLoad'])
        this.getVehicleColor().clear().type(vehicleInformation.vehicleColor)
        this.getRadioButtonLabel().contains(jobType).click()
        cy.wait(['@GETcontentLoad'])
        if (jobType == 'Impound/Tow Storage' || jobType == 'Impound/Storage') {
            this.getBussinessName().type(vehicleInformation.bussinessName)
        }
        this.getPickUpLocation().type(locationInformation.vehiclePickupLocation, { force: true })
        cy.wait(['@GETcontentLoad'])
        if (jobType == 'Impound/Tow Storage') {
            this.getDropLocationInput().type(locationInformation.vehicleDropOffLocation)
            //homePage.serviceInformationSection.getTowDropOffLocation().type(locationInformation.vehicleDropOffLocation)
        }
        if (jobType == 'Impound/Storage') {
            this.getPhoneNumber().type(vehicleInformation.phoneNumber)
        }
    },

    //for bmw
    //vinStatus active=true or expired=false
    fillVehicleInformation(vehicleInformation, partner = getPartner(), vinStatus = true, vinValid = true) {
        let vin

        if (!vinValid) {
            vin = vehicleInformation[partner].invalidVin
        } else {
            vin = vinStatus ? vehicleInformation[partner].activeVin : vehicleInformation[partner].expiredVin
        }

        if (partner == 'volvo') {
            this.getBrandName().should('have.value', getPartner().charAt(0).toUpperCase() + getPartner().slice(1))
        } else {
            this.getBrandName().type(partner + "{enter}", { force: true })
            cy.wait(['@GETcontentLoad'])
            this.getVinNumber().type(vin + "{enter}", { force: true })
            cy.wait(['@GETcontentLoad'])
            cy.wait(4000)
            homePage.addCaseModal.getNextButton().not('[disabled]').click()
            cy.wait(['@GETcontentLoad'])
        }
    }
}