import { caseHistoryModal } from '../modal_popup/case_history_modal'

const locators = require('../../../../fixtures/locators/section/add_case_locators.json')

export const addCaseRightSection = {

    getWarrantyHeader() {
        return cy.get(locators.rightDataSection.policyHeader)
    },
    getHistoryHeader() {
        return cy.get(locators.rightDataSection.historyHeader)
    },
    getSectionLabel(label) {
        return cy.get(locators.rightDataSection.estimatedPrice.sectionLabel).contains(label)
    },
    "estimatedPrice": {
        getPriceLabel(label) {
            return cy.get(locators.rightDataSection.estimatedPrice.priceLabel).contains(label)
        },
        getCalculatingText() {
            return cy.get(locators.rightDataSection.estimatedPrice.calculatingText)
        },
        getTotalDueLabel() {
            return cy.get(locators.rightDataSection.estimatedPrice.totalDueLabel)
        },
        getTotalDueAmount() {
            return cy.get(locators.rightDataSection.estimatedPrice.totalDueAmount)
        },
        getAmount(label) {
            return cy.get(locators.rightDataSection.estimatedPrice.priceLabel).contains(label).next()
        }
    },
    "providerNotes": {
        getProviderNotes() {
            return cy.get(locators.rightDataSection.providerNotes.providerNotesText)
        },
        verifyProviderNotesContent(serviceQuestionList) {
            this.getProviderNotes().should('have.length', serviceQuestionList.length)
            serviceQuestionList.forEach((serviceQuestion, index) => {
                this.getProviderNotes().eq(index).should('be.visible')
                    .and('have.text', serviceQuestion.question + ' ' + serviceQuestion.answer)
            });
        }
    },
    "warrantySection": {
        getWarrantyInfo() {
            return cy.get(locators.rightDataSection.warrantySection.warrantyInfoLabel)
        },

        getWarrantyRowList() {
            return cy.get(locators.rightDataSection.warrantySection.warrantyRow)
        },

        verifyWarrantySection() {
            this.getWarrantyRowList().should('have.length.gte', 4)
            const labelList = ["Type", "Owner","Duration", "Vehicle", "Available Services"]
            this.getWarrantyRowList().each(($el, index, $list) => {
                const label = $el.children().first().text()
                cy.wrap(label).should('be.oneOf', labelList)
                if (label == labelList[0] || label == labelList[1]) {
                    expect($el.children().first().next().text().trim()).to.match(/^[a-zA-Z ]*$/)
                } else {
                    cy.wrap($el.children().first().siblings()).should('have.length.gte', 1)
                }
            })
        }
    },
    "historySection": {
        getCaseHistoryCount() {
            return cy.get(locators.rightDataSection.historySection.totalCaseHistoryCount)
        },
        getHistoryDateList() {
            return cy.get(locators.rightDataSection.historySection.historyDateList)
        },
        getJobHistoryList() {
            return cy.get(locators.rightDataSection.historySection.jobHistoryList)
        },
        getNoJobFound() {
            return cy.get(locators.rightDataSection.historySection.noJobFound)
        },
        verifyTotalCaseListHistory() {
            this.getCaseHistoryCount().invoke('text').then((text) => {
                var totalCaseCount = parseInt(text.match(/[0-9]+/g)); 
                for(var i=1;i<totalCaseCount/10;i++){
                    this.getJobHistoryList().should('have.length.gte',10*i)
                    this.getJobHistoryList().last().scrollIntoView({offset:{ top: 200, left: 0 },duration:2000})
                    cy.wait(['@GETcontentLoad'])
                }
                this.getJobHistoryList().should('have.length.gte',totalCaseCount)
            })
        },
        clickOnJobAndverifyCaseHistory(){
            this.getJobHistoryList().first().then(($job)=>{
                const customerName=$job.children('.name').text()
                const jobCategoryAndNumber=$job.children('.jobCategoryAndNumber').text().trim()
                const jobCount=$job.children('.jobType').length
                $job.click()
                caseHistoryModal.verifyModalElements()
                caseHistoryModal.getHeader().children('.name').should('have.text',jobCategoryAndNumber.split('-')[0].trim())
                caseHistoryModal.getHeader().children('.jobNumber').should('contain.text',jobCategoryAndNumber.split('-')[1].trim())
                caseHistoryModal.getCustomerName().should('contain.text',customerName)
                caseHistoryModal.getJobList().should('have.length',jobCount)
                caseHistoryModal.getCloseIcon().click()
                caseHistoryModal.getCloseIcon().should('not.be.visible')
                cy.wrap($job).should('be.visible')
            })
        }
    }



}