const locators = require('../../../../fixtures/locators/section/add_case_locators.json')
import { homePage } from '../../home_page'

export const customerInformationSection = {
    getCallbackName() {
        return cy.get(locators.customerInfo.callbackName)
    },
    getCallbackNumber() {
        return cy.get(locators.customerInfo.callbackNumber)
    },
    getCellPhoneCheckbox() {
        return cy.get(locators.customerInfo.checkboxCellPhone)
    },
    getSendUpdateCheckbox() {
        return cy.get(locators.customerInfo.checkboxSendUpdate)
    },
    getMake() {
        return cy.get(locators.customerInfo.enterMake)
    },
    getModel() {
        return cy.get(locators.customerInfo.enterModel)
    },
    getYear() {
        return cy.get(locators.customerInfo.enterYear)
    },
    getColor() {
        return cy.get(locators.customerInfo.selectColor)
    },
    getLicensePlate() {
        return cy.get(locators.customerInfo.licensePlateNumber)
    },
    getVin() {
        return cy.get(locators.customerInfo.vinInput)
    },
    getVerifiedByCheckbox() {
        return cy.get(locators.customerInfo.verifiedByCheckbox)
    },
    getVerifyingRepresentative() {
        return cy.get(locators.customerInfo.verifyingRepresentative)
    },
    getNewCustomerLink() {
        return cy.get(locators.customerInfo.newCustomerLink)
    },
    verifyElements(isValidVIN = false) {
        this.getCallbackName().should('be.visible')
        this.getCallbackNumber().should('be.visible')
        this.getCellPhoneCheckbox().should('be.visible')
        if (isValidVIN) {
            this.getMake().should('be.visible')
            this.getModel().should('be.visible')
            this.getYear().should('be.visible')
            this.getColor().should('be.visible')
            this.getLicensePlate().should('be.visible')
            this.getVin().should('be.visible')
            this.getVerifiedByCheckbox().should('be.visible')
            this.getVerifyingRepresentative().should('be.visible')
        }
    },
    //if arguments are passed it will fill else verify
    verifyOrFillCustomerInformation(customerInformation, vehicleInformation, isVerified = false) {
        if (!customerInformation && !vehicleInformation) {
            this.getCallbackName().contains(/^[a-zA-Z ]*$/)
            this.getCallbackNumber().contains(/^[0-9]*$/)
            homePage.addCaseModal.getNextButton().not('[disabled]').click()
            cy.wait(['@GETcontentLoad'])
        } else {
            this.getCallbackName().type(customerInformation.callbackName)
            this.getCallbackNumber().type(customerInformation.callbackNumber)
            this.getMake().typeAndSelectValue(vehicleInformation.makeBmw)
            this.getModel().typeAndSelectValue(vehicleInformation.modelBmw)
            this.getYear().selectValue(vehicleInformation.year)
            this.getColor().selectValue(vehicleInformation.vehicleColor)
            this.getVin().contains(/^[a-zA-Z0-9]*$/)
            if (isVerified) {
                this.getVerifiedByCheckbox().click()
                this.getVerifyingRepresentative().not('[disabled]').type('test')
            }
            homePage.addCaseModal.getNextButton().not('[disabled]').click()
            cy.wait(['@POSTcontentLoad'])
        }
    }
}