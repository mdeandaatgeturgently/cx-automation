import { vehicleInformationSection } from './section/addcase_section/vehicle_information'
import { customerInformationSection } from './section/addcase_section/customer_information'
import { paymentInformationSection } from './section/addcase_section/payment_information'
import { additionalInformationSection } from './section/addcase_section/additional_information'
import { serviceInformationSection } from './section/addcase_section/service_information'
import { addCaseRightSection } from './section/addcase_section/right_data_section'
import { closeAddCaseModal } from './section/modal_popup/close_addcase_modal'
import { modalHeader } from './section/case_detail_section/modal_header'
import { caseInfo } from './section/case_detail_section/case_info'
import { jobDetails } from './section/case_detail_section/job_details'
import { caseDetailRightSection } from './section/case_detail_section/right_section'


const locators = require('../../fixtures/locators/home_page_locators.json')
const getPartner = () => Cypress.env('partner')

export const homePage = {

	waitForPageToLoad() {
		cy.wait(['@GETcontentLoad'])
		cy.waitUntil(() => this.getAddCaseButton().should('be.visible'), {
			errorMsg: 'Home Page is not loaded', // overrides the default error message
			timeout: 20000, // waits up to 20000 ms, default to 5000
			interval: 500 // performs the check every 500 ms, default to 200
		});
	},
	getPartnerLogo() {
		return cy.get(locators.partnerLogo)
	},
	getAddCaseButton() {
		return cy.get(locators.addCaseButton)
	},
	getNotification() {
		let locator = locators.notificationMessage
		if (getPartner() == 'bmw') {
			locator = locators[getPartner()].notificationMessage
		}
		return cy.get(locator, { timeout: 15000 })
	},
	verifyNotificationMessage(message) {
		this.getNotification().contains(message, { matchCase: false, timeout: 15000 })
	},
	searchSection: {
		getSearchJobInput() {
			return cy.get(locators.searchSection.searchJobInput)
		},
		getSearchButton() {
			return cy.get(locators.searchSection.searchButton)
		},
		getCaseStatusDropdown() {
			return cy.get(locators.searchSection.caseStatusDropdown)
		},
		getSearchResultList() {
			return cy.get(locators.searchSection.searchResultList)
		}
	},
	addCaseModal: {

		verifyModal() {
			cy.get(locators.addCaseModal.modalWindow).should('be.visible')
		},
		verifyHeader(header) {
			cy.get(locators.addCaseModal.header).should('have.text', header)
		},
		getCloseIcon() {
			return cy.get(locators.addCaseModal.closeAddCaseIcon)
		},
		//if button is in uppercase pass false
		getSubmitButton(isLowerCase = true) {
			let locator = locators.addCaseModal.submitButton
			if (!isLowerCase) {
				let arr = locator.split('\'')
				arr[1] = arr[1].toUpperCase()
				locator = arr.join('\'')
			}
			return cy.get(locator)
		},
		getNextButton() {
			return cy.get(locators.addCaseModal.nextButton)
		},
		getPreviousButton() {
			return cy.get(locators.addCaseModal.previousButton)
		},
		getDoneButton() {
			return cy.get(locators.addCaseModal.doneButton)
		},
		getActiveSection() {
			return cy.get(locators.addCaseModal.activeSection)
		},

		//Added different module as per section
		"vehicleInformation": vehicleInformationSection,
		"customerInformation": customerInformationSection,
		"serviceInformation": serviceInformationSection,
		"additionalInformation": additionalInformationSection,
		"paymentInformation": paymentInformationSection,
		"closeAddCaseModal": closeAddCaseModal,
		"rightDataSection": addCaseRightSection,

		verifyCloseAddCaseFunctionality() {
			this.getCloseIcon().click()
			this.closeAddCaseModal.verifyHeader('Close Add Case')
			this.closeAddCaseModal.verifyModalElements()
			this.closeAddCaseModal.getCloseIcon().click()
			this.closeAddCaseModal.getCloseIcon().should('not.be.visible')

			this.getCloseIcon().should('be.visible').click()
			this.closeAddCaseModal.verifyHeader('Close Add Case')
			this.closeAddCaseModal.getCancelButton().click()
			this.closeAddCaseModal.getCancelButton().should('not.be.visible')

			this.getCloseIcon().should('be.visible').click()
			this.closeAddCaseModal.verifyHeader('Close Add Case')
			this.closeAddCaseModal.getYesCloseButton().click()
			this.closeAddCaseModal.getYesCloseButton().should('not.be.visible')
			this.getCloseIcon().should('not.be.visible')
		},
		closeAddCaseSection() {
			this.getCloseIcon().click()
			this.closeAddCaseModal.verifyHeader('Close Add Case')
			this.closeAddCaseModal.getYesCloseButton().click()
			this.getCloseIcon().should('not.be.visible')
		}
	},

	"caseDetail": {
		"modalHeader": modalHeader,
		"caseInfo": caseInfo,
		"jobDetails": jobDetails,
		"rightSection": caseDetailRightSection,
		getJobNumber() {
			return cy.get(locators.caseDetail.jobNumber)
		},
		getCaseNumber() {
			return cy.get(locators.caseDetail.caseNumber)
		},
		cancelAllJobAndCloseCase() {
			this.getCaseNumber().should('be.visible').invoke('text').then((caseNumber) => {
				cy.get('.left_data').scrollTo('bottom', { duration: 2000, ensureScrollable: false })
				let jobList = []
				this.getJobNumber().each((jobNumberElement) => {
					jobList.push(jobNumberElement.text().trim().replace("- #", ""))
				})
				cy.cancelAndCloseCaseUsingAPI(caseNumber.replace("- #", ""), jobList)
			})
		}
	}

}