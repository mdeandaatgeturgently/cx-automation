
export const portalUtil = {

    /**
     * 
     * @param {String} subUrl 
     */
    verifyUrlShouldInclude(subUrl) {
        cy.url().should('include', subUrl)
    }
}