/// <reference types="Cypress" />

import { homePage } from '../../pages/home_page'

describe('Add Case E2E Scenarios', { env: { } }, () => {
    beforeEach(() => {
        cy.userLogin(Cypress.env('partner'))
        cy.getFixtures()
    })

    it('Add case for Accident Scene with existing VIN', () => {
        cy.get('@addCase').then((addCaseData) => {
            homePage.waitForPageToLoad()
            homePage.policySearch.vehicleInformation.fillVerticalVehicleInformation('Vehicle Data', addCaseData.vin, addCaseData.zip)
            homePage.getSearchPolicyButton().click()

            // ...

            //vehicle related questions
            homePage.addCaseModal.serviceInformation.getServiceQuestions().should('have.length', 9)
                .each(($el, index) => {
                    if (index == 0) {
                        cy.wrap($el).contains('Safe Location').click()
                    } else {
                        let text = index % 2 == 0 ? 'yes' : 'no'
                        cy.wrap($el).contains(text, { matchCase: false }).click()
                    }
                })
            homePage.addCaseModal.additionalInformation.vehicleOwner.fillVehicleOwnerDetails(addCaseData.customerInformation)
            homePage.addCaseModal.additionalInformation.claimInformation.fillPolicyDetails(addCaseData.claimInformation)
            homePage.addCaseModal.getSubmitButton().not('[disabled]').click({ force: true })
            cy.wait(['@POSTcontentLoad'])
            cy.wait(['@GETcontentLoad'])
            cy.wait(['@GETv2Load'])
            homePage.verifyNotificationMessage(addCaseData.jobSuccessMessage.existingCustJobSuccessMsg)
            homePage.verifyNotificationMessage('Success')
            cy.wait(3000) //To see created case in video
            homePage.caseDetail.cancelAllJobAndCloseCase()
        })
    })

    it('Add case for Impound/Tow Storage with existing VIN', () => {
        cy.get('@addCase').then((addCaseData) => {
            homePage.waitForPageToLoad()
            homePage.getAddCaseButton().click()
            cy.wait(['@GETcontentLoad'])
            cy.wait(4000)
            homePage.addCaseModal.verifyModal()
            homePage.addCaseModal.vehicleInformation.fillVerticalVehicleInformation('Impound/Tow Storage', addCaseData.vehicleInformation, addCaseData.location)
            homePage.addCaseModal.additionalInformation.vehicleOwner.fillVehicleOwnerDetails(addCaseData.customerInformation)
            homePage.addCaseModal.additionalInformation.claimInformation.fillPolicyDetails(addCaseData.claimInformation)
            homePage.addCaseModal.getSubmitButton().not('[disabled]').click({ force: true })
            cy.wait(['@POSTcontentLoad'])
            cy.wait(['@GETcontentLoad'])
            cy.wait(['@GETv2Load'])
            homePage.verifyNotificationMessage(addCaseData.jobSuccessMessage.existingCustJobSuccessMsg)
            homePage.verifyNotificationMessage('Success')
            cy.wait(3000) 
            homePage.caseDetail.cancelAllJobAndCloseCase()
        })
    })

    it('Add case for other with new VIN', () => {
        cy.get('@addCase').then((addCaseData) => {
            homePage.waitForPageToLoad()
            homePage.getAddCaseButton().click()
            cy.wait(['@GETcontentLoad'])
            cy.wait(4000)
            homePage.addCaseModal.verifyModal()
            homePage.addCaseModal.vehicleInformation.fillVerticalVehicleInformation('Other', addCaseData.vehicleInformation, addCaseData.location, false)

            // vehicle related questions
            homePage.addCaseModal.serviceInformation.getServiceQuestions().should('have.length', 9)
                .each(($el, index) => {
                    if (index == 0) {
                        cy.wrap($el).contains('Safe Location').click()
                    } else if (index == 4) {
                        cy.wrap($el).contains('Turned').click()
                    } else {
                        let text = index % 2 == 0 ? 'yes' : 'no'
                        cy.wrap($el).contains(text, { matchCase: false }).click()
                    }
                })
            homePage.addCaseModal.additionalInformation.vehicleOwner.fillVehicleOwnerDetails(addCaseData.customerInformation)
            homePage.addCaseModal.additionalInformation.claimInformation.fillPolicyDetails(addCaseData.claimInformation)
            homePage.addCaseModal.getSubmitButton().not('[disabled]').click({ force: true })
            cy.wait(['@POSTcontentLoad'])
            cy.wait(['@GETcontentLoad'])
            cy.wait(['@GETv2Load'])
            homePage.verifyNotificationMessage(addCaseData.jobSuccessMessage.existingCustJobSuccessMsg)
            homePage.verifyNotificationMessage('Success')
            cy.wait(['@GETcontentLoad'])
            homePage.caseDetail.cancelAllJobAndCloseCase()
        })
    })

})