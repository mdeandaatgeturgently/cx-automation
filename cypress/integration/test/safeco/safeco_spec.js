/// <reference types="Cypress" />

describe('Open Safeco App', () => {
    beforeEach(() => {
        cy.getFixtures()
        Cypress.config("baseUrl",Cypress.env('safecoUrl'))
    })
    it('should land on policy-detail screen', () => {
        cy.visit('policy-detail')
    });
    it('policy number length should equal to 8', () => {
        cy.get('@safeco').then((safeco) => {
            cy.get('#policyNumber').type(safeco.policyNumber).should('have.value', safeco.policyNumber)
            cy.get('#policyNumber').invoke('val').should('have.length', 8)
        })
    });
    it('zipcode length should equal 5', () => {
        cy.get('@safeco').then((safeco) => {
            cy.get('#zipcode').type(safeco.zip).should('have.value', safeco.zip)
            cy.get('#zipcode').invoke('val').should('have.length', 5)
        })
    });
    it('Look up Button should be enabled', () => {
        cy.contains('Lookup').click()
    });
    it('should select the valid vehicle', () => {
        cy.get('@safeco').then((safeco) => {
            cy.get('select').select(safeco.vehicle)
            cy.wait(1000)
            cy.get(':button').should('not.be.disabled')
            cy.get(':button').click()
        })
    });
})
describe('Chat-Bot Flow', () => {
    beforeEach(() => {
        cy.getFixtures()
    })
    it('should land on chat-bot screen', () => {
        cy.url().should('eq', Cypress.config().baseUrl)
    })
    it('should select any service', () => {
        cy.get('.chats').find('a').first().click({ timeout: 5000 })
        cy.contains('Yes').click()
    })
    it('Enter pickup location', () => {
        cy.contains('Enter another location').click()
    })
    it('navigated to other location screen to enter pickup location', () => {
        cy.get('@safeco').then((safeco) => {
            cy.url().should('eq', Cypress.config().baseUrl + 'other-location')
            cy.get('input').type(safeco.pickupLocation)
            cy.wait(1000)
            cy.get('.pac-container').find('.pac-item').first().click()
        })
    })
    it('landed back on chat-bot screen', () => {
        cy.url().should('eq', Cypress.config().baseUrl, { timeout: 5000 })
    })
    it('select drop-off location from options', () => {
        cy.wait(1000)
        cy.get('.left_info').first().click({ timeout: 5000 })
    })
    it('Enter Personal Details', () => {
        cy.get('@safeco').then((safeco) => {
            cy.get('input').type(safeco.firstName).type(safeco.enter)
            cy.wait(1000)
            cy.get('input').type(safeco.lastName).type(safeco.enter)
            cy.get('input').type(safeco.enter)
            cy.get('input').type(safeco.phoneNumber).type(safeco.enter)
            cy.contains('No').click()
            cy.get('.safeco_btn').click()
        })
    })
})
describe('Confirm service and create job', () => {
    beforeEach(() => {
        cy.getFixtures()
    })
    it('Landed on request help screen', () => {
        cy.url().should('eq', Cypress.config().baseUrl + 'request-help')
        cy.contains('Request Help').click()
    })
    it('job created and contacting providers', () => {
        cy.get('@safeco').then((safeco) => {
            cy.url().should('eq', Cypress.config().baseUrl + 'status-tracker')
            cy.get('.progress_box.mb10').find('.title').should('have.text', safeco.findProvider)
        })
    })
    it('Provider Assigned', () => {
        cy.get('@safeco').then( (safeco) => {
            cy.waitUntil(() => cy.get('.progress_box').find('.title').invoke('text').then(val => val.includes(safeco.assigned, 0)), {
                errorMsg: 'Not Assigned', // overrides the default error message
                timeout: 60000, // waits up to 60000 ms, default to 5000
                interval: 2000 // performs the check every 500 ms, default to 2000
            });
        })
    })
    it.skip('Provider On the Way', () => {
        cy.get('@safeco').then((safeco) => {
            cy.waitUntil(() => cy.get('.progress_box').find('.title').invoke('text').then(val => val.includes(safeco.onTheWay, 0)), {
                errorMsg: 'Not On the Way', // overrides the default error message
                timeout: 20000, // waits up to 20000 ms, default to 5000
                interval: 2000 // performs the check every 500 ms, default to 2000
            });
        })
    })
    it('Provider On Site', () => {
        cy.get('@safeco').then((safeco) => {
            cy.waitUntil(() => cy.get('.progress_box').find('.title').invoke('text').then(val => val.includes(safeco.onSite, 0)), {
                errorMsg: 'Not On Site', // overrides the default error message
                timeout: 30000, // waits up to 20000 ms, default to 5000
                interval: 2000 // performs the check every 500 ms, default to 2000
            });
        })
    })
    it('Provider marked complete', () => {
        cy.get('@safeco').then((safeco) => {
            cy.waitUntil(() => cy.get('.progress_box').find('.title').invoke('text').then(val => val.includes(safeco.complete, 0)), {
                errorMsg: 'Not Completed', // overrides the default error message
                timeout: 90000, // waits up to 20000 ms, default to 5000
                interval: 2000 // performs the check every 2000 ms, default to 200
            });
        })
    })
})
describe('job completed', () => {
    it('landed on complete job screen', () => {
        cy.url().should('eq', Cypress.config().baseUrl + 'complete-job')
        cy.wait(1000)
        cy.get('.bottom_fix').find('.grey-button').click()
    })
})
describe('Feedback', () => {
    it('Give ratings on rating screen', () => {
        cy.url().should('eq', Cypress.config().baseUrl + 'csat')
        cy.get('#face4').click()
        cy.get('.green_items_box').find('a').first().click()
        cy.get('.green_items_box').find('a').last().click()
        cy.get('#btnNext').find('a').click()
    })
    it('Feedback complete', () => {
        cy.url().should('eq', Cypress.config().baseUrl + 'thank')
    })
})