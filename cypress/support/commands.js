import 'cypress-wait-until';
import { loginPage } from '../integration/pages/login_page'
// Cypress.Commands.add("login", (email, password) => { ... })
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

/// <reference types="Cypress" />

Cypress.Commands.add("userLogin", (partnerName) => {

    cy.getUserDetail(partnerName).then((user) => {
        cy.visit('#/login')
        loginPage.login(user.userName, user.password)
        cy.route('/v3/ops/**').as('GETcontentLoad')
        cy.route('/v3/ops/cases/**').as('GETcaseLoad')
        cy.route('/v3/ops/configs/**').as('GETconfigLoad')        
        cy.route('PUT', '/v3/ops/**').as('PUTcontentLoad')
        cy.route('/v2/**').as('GETv2Load')
        cy.url().should('include', '#/home')
        cy.getCookie('userData').should('exist')
    })

})

Cypress.Commands.add("getFixtures", () => {
    cy.fixture('data/common/add-case.json').as('addCase')
    cy.fixture('data/common/safeco-data.json').as('safeco')
})

//Custom commands to fetch user according to environment(qa/dev) and portal
Cypress.Commands.add('getUserDetail', (platform) => {
    cy.fixture('data/users/users.' + Cypress.env('runEnv') + '.json').then((user) => {
        return user[platform]
    })
})

/**
 * Overwrite visit method to fetch url at runtime based on partner portal
 */
Cypress.Commands.overwrite('visit', (originalFn, url, options) => {

    const portal = Cypress.env('partner')

    switch (portal) {
        case 'bmw':
            url = Cypress.env('bmwUrl')
            break;

        case 'caliber':
        case 'amfam':
            url = Cypress.env('caliberUrl')
            break;

        case 'fleet':
            url = Cypress.env('fleetUrl')
            break;

        case 'safeco':
            url = Cypress.env('safecoUrl')
            break;

        case 'css':
            url = Cypress.env('cssUrl')
            break;

        case 'jlr':
            url = Cypress.env('jlrUrl')
            break;
    }

    // originalFn is the existing `visit` command that you need to call
    // and it will receive whatever you pass in here.
    //
    // make sure to add a return here!
    return originalFn(url, options)
})

Cypress.Commands.add('selectValue', {
    prevSubject: 'element'
}, (subject, text, options) => {
    cy.wrap(subject).click()
    cy.wrap(subject).next('.dropdown-menu').contains(text, { matchCase: false }).click()
    //cy.wrap(subject).next('.dropdown-menu').should('be.visible').contains(text, { matchCase: false }).click()
    //cy.get('.cssCase .dropdown-menu li').should('be.visible').contains(text, { matchCase: false }).click()
})

Cypress.Commands.add('typeAndSelectValue', {
    prevSubject: 'element'
}, (subject, text, options) => {
    cy.wrap(subject).type(text)
    cy.get('.dropdown-menu .active').contains(text).click()
})

Cypress.Commands.add('verifyText', {
    prevSubject: 'element'
}, (subject, text, options) => {
    expect(subject.text().trim()).to.be.equal(text)
})

Cypress.Commands.add('verifyDropdownList', {
    prevSubject: 'element'
}, (subject, dropDownList, options) => {
    cy.wrap(subject).click()
    cy.wrap(subject).next('.dropdown-menu').find('li .ng-binding').should('have.length', dropDownList.length)
        .each(($el, index, $list) => {
            cy.wrap($el).scrollIntoView().invoke('text').should('be.oneOf', dropDownList)
        })
    cy.wrap(subject).click()
})

Cypress.Commands.add('getAddressFromLocateVehicleAPI', (phoneNumber) => {
    cy.request({
        method: 'POST',
        url: 'https://dev01-apis.urgent.ly/v3/messages/detected-location',
        qs: {
            phone: phoneNumber
        },
        form: true,
        headers: {
            "apikey": "u9iPpKB4AArC7feZh1EEoB9zpxQz1Ixm"
        },
        body: {
            "latitude": "38.9012225",
            "longitude": "-77.2652604",
            "fullAddress": "107 Maple Avenue West, Vienna, Virginia 22180"
        }
    })
})


Cypress.Commands.add('callAPIAndGetResponse', (method, subUrl, authToken) => {
    cy.request({
        method: method,
        url: Cypress.env('apiUrl') + subUrl,
        headers: {
            'auth-token': authToken
        }
    })
})

Cypress.Commands.add('loginUsingAPI', (username, password) => {
    cy.request({
        method: 'POST',
        url: Cypress.env('apiUrl') + '/login',
        body: {
            email: username,
            password: password
        }
    }).then((resp) => {
        expect(resp.status).to.be.eq(200)
    })
})

Cypress.Commands.add('cancelAndCloseCaseUsingAPI', (caseId, jobIdList) => {
    cy.log("Calling API calls for cancelling and closing the Job[" + caseId + "," + jobIdList + "]")
    cy.getUserDetail('css').then((user) => {
        cy.loginUsingAPI(user.userName, user.password).then((resp) => {
            const authToken = resp.body.data[0].authToken
            const closeCaseAPIUrl = "/cases/" + caseId
            jobIdList.forEach((jobId) => {
                let cancelAPIUrl = "/jobs/" + jobId + "/cancel?reason=1"
                let closeAsTestAPIUrl = "/personnel/b9c47be7-0bba-4b43-8e05-f782ced38924/jobs/" + jobId + "/action/1218"
                cy.callAPIAndGetResponse("GET", cancelAPIUrl, authToken)
                cy.callAPIAndGetResponse("POST", closeAsTestAPIUrl, authToken)
            })
            cy.callAPIAndGetResponse("PUT", closeCaseAPIUrl, authToken)
        })
    })
})