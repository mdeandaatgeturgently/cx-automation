//requiring path and fs modules
const fs = require('fs');


const XML_RESULTS_PATH = "cypress/results/junit/"
removeEmptyRootSuiteFromXMLResults(XML_RESULTS_PATH);

const removeLines = (data, lines = []) => {
    return data
        .split('\n')
        .filter((val, idx) => lines.indexOf(idx) === -1)
        .join('\n');
}

function removeEmptyRootSuiteFromXMLResults(dirname) {
    console.log("Inside "+dirname+" Directory : Removing empty testsuite tag from XML reports....")
    fs.readdir(dirname, function (err, filenames) {
        if (err) console.log('error', err);
        console.log("Total XML Reports :-> "+filenames.length)
        filenames.forEach(function (filename) {
            fs.readFile(dirname + filename, 'utf-8', function (err, content) {
                if (err) console.log('error', err);
                fs.writeFile(dirname + filename, removeLines(content, [2, 3]), function (err, result) {
                    if (err) console.log('error', err);
                    console.log("Removed empty tag from :-> "+filename)
                });
            });
        });
    });
}